package com.hendisantika.springsecurityoauth2mysql;

import com.hendisantika.springsecurityoauth2mysql.model.UserInfo;
import com.hendisantika.springsecurityoauth2mysql.service.UserInfoService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.UUID;

@SpringBootApplication
public class SpringSecurityOauth2MysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityOauth2MysqlApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner init(UserInfoService userInfoService) {
        userInfoService.deleteAllUsers();
        return (evt) -> Arrays.asList(
                "user,admin,hendi,naruto,kakashi".split(",")).forEach(
                username -> {
                    UserInfo acct = new UserInfo();
                    acct.setId(UUID.randomUUID().toString());
                    acct.setUserName(username);
                    acct.setEnabled((short) 1);
                    if (username.equals("user")) acct.setPassword("password");
                    else acct.setPassword(passwordEncoder().encode("password"));
                    acct.setRole("ROLE_USER");
                    if (username.equals("admin"))
                        acct.setRole("ROLE_ADMIN");
                    userInfoService.addUser(acct);
                }
        );
    }
}
