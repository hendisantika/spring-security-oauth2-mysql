package com.hendisantika.springsecurityoauth2mysql.repository;

import com.hendisantika.springsecurityoauth2mysql.model.UserInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-security-oauth2-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/11/19
 * Time: 17.49
 */
@Repository
@Transactional
public interface UserDetailsRepository extends CrudRepository<UserInfo, String> {
    UserInfo findByUserNameAndEnabled(String userName, short enabled);

    List<UserInfo> findAllByEnabled(short enabled);

    UserInfo findById(Integer id);
//
//	@Override
//	public UserInfo save(UserInfo userInfo);

    void deleteById(Integer id);
}